var express = require('express');
var router = express.Router();

/* GET ALL PLANETS. */
router.get('/planets', function (req, res, next) {
    var db = require('../server');
    var Planet = db.Mongoose.model('planets', db.PlanetSchema, 'planets');
    Planet.find({}).lean().exec(function(error, response){
		console.log('RESPONSE: ', response);
		var newResponse = [];
		if (response && response !== undefined && response.length) {
			for (var i = 0; i < response.length; i ++) {
				var item = {};
				item.idPlaneta = response[i]._id
				item.nomePlaneta = response[i].name,
				item.climaPlaneta = response[i].climate,
				item.terrenoPlaneta = response[i].terrain,
				item.aparicoesPlanetaFilmes = response[i].films.length
				newResponse.push(item);
			}
		}
       	res.json(newResponse);
      	res.end();
    });
});

/* GET PLANET BY ID. */
router.get('/planets-by-id/:id', function (req, res, next) {
    var db = require('../server');
    var Planet = db.Mongoose.model('planets', db.PlanetSchema, 'planets');
    Planet.find({ _id: req.params.id }).lean().exec(function (error, response) {
    	var item = {};
    	if (response && response !== undefined && response.length) {
    		item.idPlaneta = response[0]._id
			item.nomePlaneta = response[0].name,
			item.climaPlaneta = response[0].climate,
			item.terrenoPlaneta = response[0].terrain,
			item.aparicoesPlanetaFilmes = response[0].films.length
    	}
        res.json(item);
        res.end();
    });
});

/* GET PLANET BY NAME. */
router.get('/planets-by-name/:name', function (req, res, next) {
    var db = require('../server');
    var Planet = db.Mongoose.model('planets', db.PlanetSchema, 'planets');
    Planet.find({ name: req.params.name }).lean().exec(function (error, response) {
        var item = {};
    	if (response && response !== undefined && response.length) {
    		item.idPlaneta = response[0]._id
			item.nomePlaneta = response[0].name,
			item.climaPlaneta = response[0].climate,
			item.terrenoPlaneta = response[0].terrain,
			item.aparicoesPlanetaFilmes = response[0].films.length
    	}
        res.json(item);
        res.end();
    });
});

/* POST ONE PLANET. */
router.post('/planets/', function (req, res, next) {
    var db = require('../server');
    var Planet = db.Mongoose.model('planets', db.PlanetSchema, 'planets');
    console.log('--REQ: ', req.body);
    var newPlanet = new Planet({ name: req.body.name, climate: req.body.climate, terrain: req.body.terrain });
    console.log('newPlanet: ', newPlanet);
    newPlanet.save(function (error) {
        if (error) {
            res.status(500).json({ error: error.message });
            res.end();
            return;
        }
        res.json(newPlanet);
        res.end();
    });
});

/* DELETE ONE PLANET. */
router.delete('/planets/:id', function (req, res, next) {
    var db = require('../server');
    var Planet = db.Mongoose.model('planets', db.PlanetSchema, 'planets');
    Planet.find({ _id: req.params.id }).deleteOne(function (error) {
        if (error) {
            res.status(500).json({ error: error.message });
            res.end();
            return;
        }
        res.json({success: true});
        res.end();
    });
});
module.exports = router;
