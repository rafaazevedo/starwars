var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/starwars', { useNewUrlParser: true });
mongoose.connect("mongodb://localhost:27017/starwars", { useNewUrlParser: true });

var planetSchema = new mongoose.Schema({
    name: String,
    climate: String,
    terrain: String,
    films: []
}, { collection: 'planets' }
);

module.exports = { Mongoose: mongoose, PlanetSchema: planetSchema }